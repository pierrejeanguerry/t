#/usr/bin/python3.10
# Salson Enzo - 2023
import pandas as pd


def convert():
    """
    Conversion d'un fichier texte en une liste de lignes
    :return: list
    """
    with open("lieux.txt",'r') as lieux:
        lieu = lieux.read()
        lieu = lieu.split('\n')
        return lieu

df = pd.read_csv("cd.csv")
df["Numéro de Téléphone"] = "0" + df["Numéro de Téléphone"].astype(str)
lieux = {}

for lieu in convert():
    cols = ["Nom", "Prénom", "Numéro de Téléphone","Jour", "Horaire", "Informations complémentaires"]
    data = df.loc[df["Lieu"] == lieu, cols]
    data = data.sort_values("Jour")
    lieux[lieu] = data

with pd.ExcelWriter("Excel/EDT.xlsx", engine="xlsxwriter") as writer:
    for lieu, data in lieux.items():
        data.to_excel(writer, sheet_name=lieu, index=False)
        worksheet = writer.sheets[lieu]
        worksheet.set_column('C:C', 30)
        worksheet.set_column('E:E', 30)
        worksheet.set_column('F:F', 30)
        worksheet.set_default_row(25)

